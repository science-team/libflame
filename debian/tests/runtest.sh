#!/bin/sh
set -e
mkdir -p test/obj
make -C test LINKER=gcc LIBFLAME=/usr/lib/$(dpkg-architecture -qDEB_HOST_MULTIARCH)/libflame.so
cd test; ./test_libflame.x; cd ..
exit 0
