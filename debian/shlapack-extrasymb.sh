#!/bin/bash
set -e
export LANG=C

flame_all_obj_list=ar_obj_list
cat $flame_all_obj_list \
	| awk '/obj\/.*\/src\/map\/lapack2flamec\/f2c\/c\//{print}' \
	| sed -e 's@/@ @g' \
	| awk '{print $NF}' \
	| sort > debian/flame.list
echo wrote debian/flame.list

lapack_pic="$(dpkg -L liblapack-dev | grep liblapack_pic.a)"
ar t $lapack_pic | sort > debian/lapack.list
echo wrote debian/lapack.list

diff -ru debian/flame.list debian/lapack.list \
	| awk '(/^+/ && NF==1){print}' \
	| sed -e 's/^+//g' > debian/extrasymb.list

