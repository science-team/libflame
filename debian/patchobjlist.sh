#!/bin/bash
mkdir -p lapack-pic/
cd lapack-pic; ar x /usr/lib/$(dpkg-architecture -qDEB_HOST_MULTIARCH)/liblapack_pic.a
cd ..
(cat debian/exclude.list | xargs rm) || true

objlist=( $(cat debian/extrasymb.list | awk '{print "lapack-pic/" $0}') )
for obj in ${objlist[@]}; do
	if test -r $obj; then
		echo $obj
	fi
done
